### How do I get set up? ###

In the server-access directory, run pip install requirements.txt and run python setup.py install

### Instructions ###

To get all users: zerosm get --user/-u   
To get all servers: zerosm get --server/-s   
To get all users from a specific server: zerosm get --user all --server <servername>   
To find a user from every server: zerosm find --user <username>   
To add a user on a server: zerosm add --user <username> --server <servername>   
To delete a user from a server: zerosm delete --user <username> --server <servername>    
To delete a user from all servers : zerosm delete --user <username> --server <servername>   