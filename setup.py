#!/usr/bin/python
from distutils.core import setup

requires = ['paramiko(>=2.0)', 'boto3(>=1.4.7)']

setup(name='zerosm',
      version='1.0',
      description='server user management utility',
      author='Juwon',
      author_email='kjw8384@hotmail.com',
      scripts=['zerosm'],
      requires=requires
      )
