#!/usr/bin/python
import sys
import boto3
import argparse
from paramiko.client import SSHClient
from functools import wraps

parser = argparse.ArgumentParser()
parser.add_argument("command", help=": add, delete, get and find")
parser.add_argument("-u", "--user", nargs='?', const=1)
parser.add_argument("-s", "--server", nargs='?', const=1)
args = parser.parse_args()

# Check if the command is appliable
if args.command not in ('add', 'delete', 'get', 'find'):
    print('No such a command ' + args.command)
    sys.exit()

# Variables required to do queries on AWS and servers
ec2 = boto3.client('ec2')
dynamo = boto3.client('dynamodb')
ssh = SSHClient()


def dynamo_scan(dynamo, attr_name, attr_value, compare_type):
    db_response = dynamo.scan(
        TableName='ServerAccessKey',
        Select='ALL_ATTRIBUTES',
        ScanFilter={
            attr_name: {
                'AttributeValueList': [
                    {
                        'S': attr_value
                    }
                ],
                'ComparisonOperator': compare_type
            }
        }
    )
    return db_response


def get_ec2_instance_by_name(server_name):
    if server_name == 'all':
        result = ec2.describe_instances()
    else:
        result = ec2.describe_instances(
            Filters=[
                {
                    'Name': 'tag:Name',
                    'Values': [
                        server_name
                    ]
                }
            ]
        )
    return result


def check_server_access(ip, ssh):
    try:
        ssh.load_system_host_keys()
        ssh.connect(ip['address'], username='ubuntu', timeout=2, auth_timeout=2, banner_timeout=2)
        return True
    except:
        print('Access denied for the server %s' % ip['name'])
        return False


def get_ssh_key_from_server(ssh):
    cmd = 'cat .ssh/authorized_keys'
    stdin, stdout, stderr = ssh.exec_command(cmd)
    users= [line.strip() for line in stdout]
    return users


def check_user_exists_on_server(ssh, ssh_key):
    cmd = "grep '%s' .ssh/authorized_keys" % ssh_key
    if sum(1 for _ in ssh.exec_command(cmd)[1].read()) > 0:
        return True
    else:
        return False


# Get command section
if args.command == 'get' and (args.user is None) and args.server == 1:
    response = ec2.describe_instances()
    for instance in response['Reservations']:
        if instance['Instances'][0].get('Tags'):
            print(instance['Instances'][0]['Tags'][0]['Value'])
    sys.exit()

if args.command == 'get' and args.user == 1 and (args.server is None):
    scan_result = dynamo_scan(dynamo, 'group', 'zeroweb', 'EQ')
    for item in scan_result['Items']:
        print(item['name']['S'])
    sys.exit()

if args.command == 'get' and args.user == 'all' and args.server != 1:
    ec2_response = get_ec2_instance_by_name(args.server)
    if not ec2_response['Reservations']:
        print('No such a server with the given name')
        sys.exit()
    server_ip = ec2_response['Reservations'][0]['Instances'][0]['PublicIpAddress']
    ip = dict(name=args.server, address=server_ip)
    if check_server_access(ip, ssh):
        print('Current users on server %s:' % args.server)
        for ssh_key in get_ssh_key_from_server(ssh):
            scan_result = dynamo_scan(dynamo, 'key', ssh_key, 'EQ')
            print(scan_result['Items'][0]['name']['S'])
    sys.exit()


# Find command section
if args.command == 'find' and args.user != 1:
    response = ec2.describe_instances()
    ips = []
    for instance in response['Reservations']:
        public_ip = ""
        server_name = "No name"
        if instance['Instances'][0].get('PublicIpAddress'):
            public_ip = instance['Instances'][0]['PublicIpAddress']
        if instance['Instances'][0].get('Tags'):
            server_name = instance['Instances'][0]['Tags'][0]['Value']
        ips.append(dict(address=public_ip, name=server_name))

    scan_result = dynamo_scan(dynamo, 'name', args.user, 'IN')
    ssh_key = scan_result['Items'][0]['key']['S']
    cmd = "grep '%s' .ssh/authorized_keys" % ssh_key
    on_servers = [ip['name'] for ip in ips
                  if check_server_access(ip, ssh) and check_user_exists_on_server(ssh, ssh_key)]

    print('%s is on :' % args.user)
    for server in on_servers:
        print(server)
    sys.exit()


# Add and Delete command section
# Check if all the arguments are given
if not (args.user != 1 and args.server != 1):
    print("Missing Arguments -u/--user or -s/--server")
    sys.exit()

# Get the server_ip and the name of a given server
ec2_response = get_ec2_instance_by_name(args.server)

# Get the public key and the name of a given user
scan_result = dynamo_scan(dynamo, 'name', args.user, 'IN')

# Check if there are matched server and user
if not ec2_response['Reservations']:
    print('No server with the given name was found"' + args.server + '"')
    sys.exit()
if not scan_result['Items']:
    print('No such a user')
    sys.exit()

server_name = ec2_response['Reservations'][0]['Instances'][0]['Tags'][0]['Value']
server_ip = ec2_response['Reservations'][0]['Instances'][0]['PublicIpAddress']
user_name = scan_result['Items'][0]['name']['S']
ssh_key = scan_result['Items'][0]['key']['S']


# Build a command based on a given command
cmd = ""
if args.command == 'add':
    cmd = "echo %s >> .ssh/authorized_keys && cat .ssh/authorized_keys" % ssh_key.strip()
elif args.command == 'delete':
    ssh_key = ssh_key.replace('/', '\/')
    cmd = "sed -i '/%s/d' .ssh/authorized_keys && cat .ssh/authorized_keys" % ssh_key

# Do the action on the server or servers
for server in ec2_response['Reservations']:
    public_ip = ""
    server_name = ""
    if server['Instances'][0].get('PublicIpAddress'):
        public_ip = server['Instances'][0]['PublicIpAddress']
    if server['Instances'][0].get('Tags'):
        server_name = server['Instances'][0]['Tags'][0]['Value']
    ip = dict(name=server_name, address=public_ip)
    if not check_server_access(ip, ssh):
        continue
    stdin, stdout, stderror = ssh.exec_command(cmd)
    for line in stderror:
        print(line)
    print('Current users on %s:' % server_name)
    for ssh_key in stdout:
        scan_result = dynamo_scan(dynamo, 'key', ssh_key.strip(), 'EQ')
        print(scan_result['Items'][0]['name']['S'])
